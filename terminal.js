const cmd = require('node-cmd');
const Promise = require('bluebird');

runAsync = Promise.promisify(cmd.get, { multiArgs: true, context: cmd });

getPackages = ( basedir, distname ) => {

  const command = `reprepro -b ${basedir} list ${distname}`;
  const result = runAsync(command).then(data => {
    const res = data[0].split('\n').filter( x => x ).map( str => { 
      const [_, name, version ] = str.split(' ');
      return { name, version };
    });
    return res;
  }).catch(err => {
    console.error(err);
  });

  return result;

};

removePackage = ( basedir, distname, packagename ) => {

  const command = `reprepro -b ${basedir} remove ${distname} ${packagename}`;
  runAsync(command).catch( console.error.bind(console) );

};

movePackage = ( basedir, destinationdist, sourcedist, packagename ) => {

  const command = `reprepro -b ${basedir} copy ${destinationdist} ${sourcedist} ${packagename}`;
  runAsync(command).catch( console.error.bind(console) );

};

getDistsInfo = ( basedir ) => {
  const command = `cat ${basedir}/conf/distributions`;
  const dist = runAsync(command).then(data =>
    data.split('\n')
    .filter(str => str.toLowerCase().includes('codename'))
    .map(str => ({ codename: str.split(': ')[1] }) )
  );
  return dist;
};


/*
const cmd = require('node-cmd');
const Promise = require('bluebird');

getAsync = Promise.promisify(cmd.get, { multiArgs: true, context: cmd });

getListFromDist = distname => {
  let command = `reprepro -b ~/repo/ list ${distname}`;
  getAsync(command).then(data => {
    const res = data[0].split('\n').filter( x => x ).map( str => { 
      const [_, name, version ] = str.split(' ');
      return { name, version };
    });
    // let resJson = JSON.stringify(res);
    return res;
  }).catch(err => {
    return (err);
  });
};

module.exports = {
  getListFromDist: getListFromDist,
};
*/