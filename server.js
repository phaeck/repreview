// const http = require('http');
//
// const hostname = '127.0.0.1';
// const port = 3000;
//
// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World\n');
// });
//
// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

var Koa = require('koa');
var Router = require('koa-router');
var serve = require('koa-static');

var app = new Koa();
var router = new Router();

const api = require('terminal');

router.get('/', serve('./static/index.html'));

router.get('/users/', async (ctx, next) => {

  var a = await api.getListFromDist('trusty');

  ctx.request.type=='application/json';
  ctx.body = a;
  
});

app.use(serve('./static'))
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);

console.debug('Server started on localhost:3000');
